"""
band-name-generator.py

Generates a band name from the city you grew up in and your pet's name.
"""

print('Welcome to the Band Name Generator.')
city = input('What\'s the name of the city you grew up in?\n')
pet = input('What\'s your pet\'s name?\n')
print(f'Your band name could be {city} {pet}.')