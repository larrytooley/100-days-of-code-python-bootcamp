print('Welcome to the tip calculator.')
total_bill = float(input('What was the total bill? $'))
tip_percent = int(input('What percentage tip would you like to give? 10, 12, or 15? '))
num_of_people = int(input('How many people to split the bill? '))
total_per_person = round(total_bill / num_of_people * (1 + tip_percent / 100), 2)

print(f'Each person should pay: ${total_per_person:.2f}')